/**
 * 
 */
package com.app.Mutant.mongoDB;

import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

/**
 * @author san_c
 *
 */
@EnableMongoRepositories(basePackages = {"com.app.Mutant.repository"})
public class MongoDbConfig {

}

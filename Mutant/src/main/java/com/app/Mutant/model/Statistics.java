/**
 * 
 */
package com.app.Mutant.model;

/**
 * @author san_c
 *
 */
public class Statistics {
	
	private long countMutations;
	private long countNoMutations;
	private double ratio;
	/**
	 * @return the countMutations
	 */
	public long getCountMutations() {
		return countMutations;
	}
	/**
	 * @param countMutations the countMutations to set
	 */
	public void setCountMutations(long countMutations) {
		this.countMutations = countMutations;
	}
	/**
	 * @return the countNoMutations
	 */
	public long getCountNoMutations() {
		return countNoMutations;
	}
	/**
	 * @param countNoMutations the countNoMutations to set
	 */
	public void setCountNoMutations(long countNoMutations) {
		this.countNoMutations = countNoMutations;
	}
	/**
	 * @return the ratio
	 */
	public double getRatio() {
		return ratio;
	}
	/**
	 * @param ratio the ratio to set
	 */
	public void setRatio(double ratio) {
		this.ratio = ratio;
	}
	
	
}

package com.app.Mutant.model;

import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "DNA")
public class DNA {
	@Id
	private String id;
	private List<String> matrix;
	private String code;
	private boolean isMutant;

	
	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * @return the matrix
	 */
	public List<String> getMatrix() {
		return matrix;
	}

	/**
	 * @param matrix the matrix to set
	 */
	public void setMatrix(List<String> matrix) {
		this.matrix = matrix;
	}

	/**
	 * @return the code
	 */
	public String getCode() {
		return code;
	}

	/**
	 * @param code the code to set
	 */
	public void setCode(String code) {
		this.code = code;
	}

	/**
	 * @return the isMutant
	 */
	public boolean isMutant() {
		return isMutant;
	}

	/**
	 * @param isMutant the isMutant to set
	 */
	public void setMutant(boolean isMutant) {
		this.isMutant = isMutant;
	}
	
	@Override
    public String toString() {
        return "DNA{" +
                "id='" + id + '\'' +
                ", matrix='" + matrix + '\'' +
                ", code='" + code + '\'' +
                ", isMutant=" + isMutant +
                '}';
    }
	
		
}

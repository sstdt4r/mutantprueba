package com.app.Mutant.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.app.Mutant.model.DNA;

public interface MutantRepository extends MongoRepository<DNA, String> {
	
	

}

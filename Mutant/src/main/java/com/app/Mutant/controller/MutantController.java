package com.app.Mutant.controller;

import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.app.Mutant.model.DNA;
import com.app.Mutant.model.Statistics;
import com.app.Mutant.service.MutantService;

@RestController
@RequestMapping(value = "/mutant")
public class MutantController {
	
	@Autowired
	private MutantService service;
	
	@RequestMapping(method = RequestMethod.POST, produces = "application/json", 
            value = "/hasMutation")
	public DNA hasMutation(@RequestParam List<String> dnaChain) {
		return service.hasMutation(dnaChain);	
	}
	
	@RequestMapping(method = RequestMethod.GET, produces = "application/json", 
            value = "/getStats")
	public Statistics getStats() {
	    return service.getStats();
	 }

}

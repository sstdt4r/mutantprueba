package com.app.Mutant.utility;

import java.util.List;

import com.app.Mutant.model.DNA;
import com.app.Mutant.model.Statistics;

public class MutantUtility {
	
	public boolean isMutant(String[] dna){
        int count = 0;
        boolean isHorizontal = false;
        boolean isVertical = false;
        boolean isDiagonalFowards = false;
        boolean isDiagonalBackwards = false;

        outer:
        for (int i = 0; i < dna.length; i++) {

            for (int j = 0; j < dna[i].length(); j++) {
                if ((j + 3) < dna.length) {
                    isHorizontal = isMutant(dna[i].charAt(j), dna[i].charAt(j + 1),
                            dna[i].charAt(j + 2), dna[i].charAt(j + 3));
                }
                if ( ((j + 3) < dna.length) &&  ((i + 3) < dna.length)) {
                    isDiagonalFowards = isMutant(dna[i].charAt(j), dna[i + 1].charAt(j + 1),
                            dna[i + 2].charAt(j + 2), dna[i + 3].charAt(j + 3));
                }
                if ( ((j + 3) < dna.length) &&  ((i + 3) < dna.length)) {
                    isDiagonalBackwards = isMutant(dna[i].charAt(j+3), dna[i+1].charAt(j+2),
                            dna[i+2].charAt(j+1), dna[i+3].charAt(j));
                }
                if ((i + 3) < dna.length) {
                    isVertical = isMutant(dna[i].charAt(j), dna[i + 1].charAt(j),
                            dna[i + 2].charAt(j), dna[i + 3].charAt(j));
                }
                boolean valid = validate(isHorizontal, isDiagonalFowards, isDiagonalBackwards, isVertical);
                if (valid) count++;
                if (count >= 2) break outer;
            }

        }

        if (count >= 2 ) {
            return true;
        } else {
            return false;
        }
    }

    private boolean validate(boolean isHorizontal, boolean isDiagonalFowards, boolean isDiagonalBackwards, boolean isVertical ) {
         if (isHorizontal
                || isVertical
                || isDiagonalBackwards
                || isDiagonalFowards) {
            return true;
        } else {
            return false;
        }

    }

    private boolean isMutant(char a, char b, char c, char d){
        if (a == b && b == c && c == d) {
            return true;
        }
        return false;
    }
	
	public Statistics calculateRatio(List<DNA> mutantCount) {
		Statistics stats = new Statistics();
		long hasMutation = mutantCount.stream()
				  .filter(c -> c.isMutant())
				  .count();
		stats.setCountMutations(hasMutation);
		long hasNoMutation = mutantCount.stream()
				  .filter(c -> !c.isMutant())
				  .count();
		stats.setCountNoMutations(hasNoMutation);	
		stats.setRatio(hasMutation/hasNoMutation);
		return null;
	}

}

/**
 * 
 */
package com.app.Mutant.service;

import java.util.List;

import com.app.Mutant.model.DNA;
import com.app.Mutant.model.Statistics;

/**
 * @author san_c
 *
 */
public interface MutantService {
	
	public DNA hasMutation(List<String> dna);
	public Statistics getStats();
}

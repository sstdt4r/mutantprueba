/**
 * 
 */
package com.app.Mutant.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.app.Mutant.model.DNA;
import com.app.Mutant.model.Statistics;
import com.app.Mutant.repository.MutantRepository;
import com.app.Mutant.utility.MutantUtility;

/**
 * @author san_c
 *
 */
@Service
public class MutantServiceImpl implements MutantService {
	
	private static final String HAS_MUTATION = "200"; 
	private static final String HAS_NO_MUTATION = "403"; 
	@Autowired
	private MutantRepository repository;

	@Override
	public DNA hasMutation(List<String> dna) {
		
		boolean isMutant = new MutantUtility().isMutant(dna.stream()
				 .toArray(String[]::new));
		DNA dnaObject = new DNA();
		dnaObject.setMatrix(dna);
		dnaObject.setMutant(isMutant);
		if (isMutant) {
			dnaObject.setCode(HAS_MUTATION);
		} else {
			dnaObject.setCode(HAS_NO_MUTATION);
		}
	
		return saveDNA(dnaObject);
	}
	
	private DNA saveDNA(DNA dna) {
		return repository.save(dna);
	}

	@Override
	public Statistics getStats() {
		List<DNA> mutantCount = repository.findAll();
		Statistics stats = new MutantUtility().calculateRatio(mutantCount);
		return stats;
	}

}

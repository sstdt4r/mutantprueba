#ARCA SEGUROS PRUEBA

Nombre del Proyecto: Mutant Prueba

Autenticacion:
Desde SOAPUI
1. Elegir Authentication and Security-related setting
2. Click en Agregar
3. Tipo Basica
 usuario: admin
 password: password
 
API
GET
/mutant/hello

Inputs: ninguno
Output: String

POST
/mutant/hasMutation

Inputs: Arreglo de Strings
Output: Objeto DNA
Ej:
{
   "id": "5ebf46a9ca2f1a63d6b4d929",
   "matrix":    [
      "\"ATGCGT\"",
      "\"GAGTGC\"",
      "\"TTATGT\"",
      "\"AGAAGG\"",
      "\"CCCCTT\"",
      "\"TCACTG\""
   ],
   "code": "200",
   "mutant": true
}

GET
/mutant/getStats

Inputs: ninguno
Output: Objeto Statistics
Ej:
{
   "countMutations": 14,
   "countNoMutations": 0,
   "ratio": 0
}

POST
/mutant/httpHasMutation

Inputs: Arreglo de Strings
Output: String

Endpoint deployado en Azure: https://atotallydifferentname12345maybe.azurewebsites.net
GitLab URL: https://gitlab.com/sstdt4r/mutantprueba.git